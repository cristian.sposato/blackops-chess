package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.game.GameService
import kotlinx.coroutines.flow.Flow

class GetGames(private val gameService: GameService) {

    operator fun invoke(playerId: String): Flow<Game> {
        return gameService.getGames(playerId)
    }
}
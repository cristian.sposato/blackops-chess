package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.game.GameService
import kotlinx.coroutines.flow.Flow

class GetBoard(private val gameService: GameService) {

    operator fun invoke(gameId: String): Flow<Board> {
        return gameService.getBoard(gameId)
    }

}
package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.game.GameService

class Move(private val gameService: GameService) {

    operator fun invoke(gameId: String, currentPlayerId: String, move: String): Boolean {
        return gameService.move(gameId, currentPlayerId, move)
    }
}
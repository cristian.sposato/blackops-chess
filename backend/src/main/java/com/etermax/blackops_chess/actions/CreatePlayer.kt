package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.domain.player.PlayerService

class CreatePlayer(private val playerService: PlayerService) {
    operator fun invoke(): Player {
        return playerService.createPlayer()
    }
}
package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.game.GameError
import com.etermax.blackops_chess.domain.game.GameService
import com.etermax.blackops_chess.utils.Result

class CreateGame(private val gameService: GameService) {

    operator fun invoke(playerId1: String, playerId2: String): Result<Game, GameError> {
        return gameService.createGame(playerId1, playerId2)
    }
}

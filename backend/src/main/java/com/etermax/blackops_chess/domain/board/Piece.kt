package com.etermax.blackops_chess.domain.board

interface Piece {
    val color: Color

    fun canMove(
        board: List<List<Piece>>,
        originCoordinates: Pair<Int, Int>,
        destinationCoordinates: Pair<Int, Int>
    ): Boolean

    enum class Color {
        BLACK,
        WHITE,
        NONE,
    }
}
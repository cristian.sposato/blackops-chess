package com.etermax.blackops_chess.domain.board

import kotlin.math.abs

class RookPiece(override val color: Piece.Color) : Piece {
    override fun canMove(
        board: List<List<Piece>>,
        originCoordinates: Pair<Int, Int>,
        destinationCoordinates: Pair<Int, Int>
    ): Boolean {
        return when {
            originCoordinates.first == destinationCoordinates.first -> {
                moveY(board, originCoordinates, destinationCoordinates)
            }
            originCoordinates.second == destinationCoordinates.second -> {
                moveX(board, originCoordinates, destinationCoordinates)
            }
            else -> false
        }
    }

    private fun moveX(
        board: List<List<Piece>>,
        originCoordinates: Pair<Int, Int>,
        destinationCoordinates: Pair<Int, Int>,
    ): Boolean {
        if (areAdjacent(originCoordinates.first, destinationCoordinates.first)) return true

        val x = if (destinationCoordinates.first - originCoordinates.first > 0) 1 else -1
        val origin = originCoordinates.first + x
        val destination = destinationCoordinates.first - x
        val range = if ((origin) > (destination)) origin..destination else origin downTo destination
        for (i in range) {
            if (board[i][originCoordinates.second] !is EmptyPiece) return  false
        }
        return true
    }

    private fun moveY(
        board: List<List<Piece>>,
        originCoordinates: Pair<Int, Int>,
        destinationCoordinates: Pair<Int, Int>,
    ): Boolean {
        if (areAdjacent(originCoordinates.second, destinationCoordinates.second)) return true

        val y = if (destinationCoordinates.second - originCoordinates.second > 0) 1 else -1
        val origin = originCoordinates.second + y
        val destination = destinationCoordinates.second - y
        val range = if ((origin) > (destination)) origin downTo destination else origin..destination
        for (i in range) {
            if (board[originCoordinates.first][i] !is EmptyPiece) return  false
        }
        return true
    }

    private fun areAdjacent(first: Int, second: Int) = abs(second - first) == 1

    override fun toString(): String {
        // shame on this...sorry Uncle Bob
        return "R" + if (this.color == Piece.Color.BLACK) "B" else "W"
    }
}
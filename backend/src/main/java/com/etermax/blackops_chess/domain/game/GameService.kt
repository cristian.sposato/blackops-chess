package com.etermax.blackops_chess.domain.game

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.board.BoardGenerator
import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.domain.player.PlayerService
import com.etermax.blackops_chess.infrastructure.IdGenerator
import com.etermax.blackops_chess.utils.Result
import kotlinx.coroutines.flow.*

class GameService(
    private val gameRepository: GameRepository,
    private val playerService: PlayerService,
    private val idGenerator: IdGenerator,
    private val boardGenerator: BoardGenerator,
) {

    private val gameFlow = MutableStateFlow(emptyGame())
    private val moveFlow = MutableStateFlow(emptyGame())

    fun createGame(playerId1: String, playerId2: String): Result<Game, GameError> {

        if (playerId1 == playerId2) return Result.Error(GameError)

        val player1 = playerService.findById(playerId1)
        val player2 = playerService.findById(playerId2)

        if (player1 is Result.Ok && player2 is Result.Ok) {
            val gameId = idGenerator.generate()
            val board = boardGenerator.generate()
            val game = Game(gameId, player1.value, player2.value, board, player1.value)
            gameRepository.save(game)
            gameFlow.value = game
            return Result.Ok(game)
        }

        return Result.Error(GameError)
    }

    fun getGames(playerId: String): Flow<Game> {
        return flow {
            gameRepository.findByPlayerId(playerId).forEach { game ->
                emit(game)
            }
            gameFlow.collect { game ->
                if (playerId == game.player1.id || playerId == game.player2.id)
                    emit(game)
            }
        }.distinctUntilChanged()
    }

    fun move(gameId: String, currentPlayer: String, move: String): Boolean {
        return when(val result = gameRepository.findById(gameId)) {
            is Result.Ok -> {
                val game = result.value
                val moved = game.move(move.uppercase(), currentPlayer)
                if (moved) {
                    val updatedGame = game.copy(currentPlayer = game.newCurrentPlayer())
                    gameRepository.update(updatedGame)
                    moveFlow.value = updatedGame
                }
                return moved
            }
            is Result.Error -> false
        }
    }

    fun getBoard(gameId: String): Flow<Board> {
        return flow {
            when (val game = gameRepository.findById(gameId)) {
                is Result.Ok -> emit(game.value.getBoard())
                is Result.Error -> Unit
            }
            moveFlow.collect { game ->
                if (game.id == gameId) emit(game.getBoard())
            }
        }
    }

    private fun emptyGame() = Game("", Player(""), Player(""), Board(), Player(""))
}
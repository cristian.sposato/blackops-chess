package com.etermax.blackops_chess.domain.player

import com.etermax.blackops_chess.utils.Result

interface PlayerRepository {
    fun findById(id: String): Result<Player, PlayerError>
    fun save(player: Player)
}
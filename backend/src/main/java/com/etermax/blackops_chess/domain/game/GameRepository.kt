package com.etermax.blackops_chess.domain.game

import com.etermax.blackops_chess.utils.Result

interface GameRepository {
    fun save(game: Game)
    fun findById(id: String): Result<Game, GameError>
    fun findByPlayerId(playerId: String): List<Game>
    fun update(game: Game)
}


package com.etermax.blackops_chess.domain.player

import com.etermax.blackops_chess.infrastructure.IdGenerator
import com.etermax.blackops_chess.utils.Result

class PlayerService(
    private val idGenerator: IdGenerator,
    private val repository: PlayerRepository
) {
    fun createPlayer(): Player {
        val player = Player(idGenerator.generate())
        repository.save(player)
        return player
    }

    fun findById(id: String): Result<Player, PlayerError> {
        return repository.findById(id)
    }
}

package com.etermax.blackops_chess.domain.board

import com.etermax.blackops_chess.domain.board.Piece.*

object EmptyPiece : Piece {
    override val color = Color.NONE

    override fun canMove(
        board: List<List<Piece>>,
        originCoordinates: Pair<Int, Int>,
        destinationCoordinates: Pair<Int, Int>
    ) = false

    override fun toString(): String {
        return ""
    }
}
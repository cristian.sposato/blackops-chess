package com.etermax.blackops_chess.domain.game

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.board.Piece
import com.etermax.blackops_chess.domain.player.Player

data class Game(
    val id: String,
    val player1: Player,
    val player2: Player,
    private val board: Board,
    val currentPlayer: Player,
) {
    val pieces: List<List<Piece>> = board.pieces

    fun getBoard() = board

    fun move(move: String, currentPlayerId: String): Boolean {
        if (currentPlayerId == currentPlayer.id) {
            return board.move(move)
        }
        return false
    }

    fun newCurrentPlayer() = if (currentPlayer == player1) player2 else player1
}

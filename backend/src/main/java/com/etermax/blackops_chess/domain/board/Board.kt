package com.etermax.blackops_chess.domain.board

import com.etermax.blackops_chess.utils.emptyBoard

class Board(val pieces: MutableList<MutableList<Piece>> = chessPieces()) {

    companion object {
        val LETTERS = mapOf(
            'A' to 0,
            'B' to 1,
            'C' to 2,
            'D' to 3,
            'E' to 4,
            'F' to 5,
            'G' to 6,
            'H' to 7
        )

        fun chessPieces() = emptyBoard(8) {
            it[0][0] = RookPiece(Piece.Color.WHITE)
            it[0][7] = RookPiece(Piece.Color.WHITE)
            it[7][0] = RookPiece(Piece.Color.BLACK)
            it[7][7] = RookPiece(Piece.Color.BLACK)
        }
    }

    fun move(move: String): Boolean {
        val origin = move.substring(0, 2)
        val destination = move.substring(2, 4)

        if (!isValidMove(origin, destination)) return false

        val originCoordinates = getCoordinates(origin)
        val destinationCoordinates = getCoordinates(destination)

        val piece = getPiece(originCoordinates)
        val destinationPiece = getPiece(destinationCoordinates)

        if (piece.canMove(pieces, originCoordinates, destinationCoordinates, )) {
            if (!isDifferentColor(piece, destinationPiece)) return false
            doMove(originCoordinates, destinationCoordinates)
            return true
        }
        return false
    }

    private fun isDifferentColor(origin: Piece, destination: Piece): Boolean {
        return origin.color != destination.color
    }

    private fun getPiece(coordinates: Pair<Int, Int>): Piece {
        val (x, y) = coordinates
        return pieces[x][y]
    }

    private fun doMove(
        originCoordinates: Pair<Int, Int>,
        destinationCoordinates: Pair<Int, Int>,
    ) {
        val (xOrigin, yOrigin) = originCoordinates
        val (xDestination, yDestination) = destinationCoordinates
        pieces[xDestination][yDestination] = pieces[xOrigin][yOrigin]
        pieces[xOrigin][yOrigin] = EmptyPiece
    }

    private fun isValidMove(origin: String, destination: String): Boolean {
        val regex = "([A-H])[1-8]".toRegex()
        return origin != destination && regex.matches(origin) && regex.matches(destination)
    }

    private fun getCoordinates(chessNotation: String): Pair<Int, Int> {
        val letter = chessNotation[0]
        return Pair(chessNotation[1].toString().toInt() - 1, LETTERS[letter]!!)
    }
}
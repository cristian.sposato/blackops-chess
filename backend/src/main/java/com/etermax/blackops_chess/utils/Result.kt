package com.etermax.blackops_chess.utils

sealed class Result<out T, out E> {
    class Ok<T>(val value: T) : Result<T, Nothing>()
    class Error<E>(val error: E) : Result<Nothing, E>()
}
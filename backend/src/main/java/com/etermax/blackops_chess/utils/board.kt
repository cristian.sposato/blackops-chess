package com.etermax.blackops_chess.utils

import com.etermax.blackops_chess.domain.board.EmptyPiece
import com.etermax.blackops_chess.domain.board.Piece


fun emptyBoard(
    size: Int = 2,
    block: (MutableList<MutableList<Piece>>) -> Unit
): MutableList<MutableList<Piece>> {
    return MutableList<MutableList<Piece>>(size) {
        MutableList(size) { EmptyPiece }
    }.apply {
        block(this)
    }
}
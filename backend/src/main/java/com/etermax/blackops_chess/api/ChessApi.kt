package com.etermax.blackops_chess.api

import com.etermax.blackops_chess.actions.*
import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.board.Piece
import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.game.GameError
import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.utils.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.logging.Logger

class ChessApi(
    private val createPlayer: CreatePlayer,
    private val createGame: CreateGame,
    private val getGames: GetGames,
    private val move: Move,
    private val getBoard: GetBoard,
) : BoardStateGrpcKt.BoardStateCoroutineImplBase() {

    private val logger = Logger.getLogger("chess-api")

    override suspend fun createPlayer(request: Contract.Client): Contract.Player {
        logger.info("Create player request.")

        val player = createPlayer()
        logger.info("Create player response. Player:$player")
        return player.toPlayer()
    }


    override suspend fun createNewGame(request: Contract.Players): Contract.GameResponse {
        val player1 = request.player1.playerId
        val player2 = request.player2.playerId
        logger.info("Create game request with params: $request")

        val game = createGame(player1, player2)
        logger.info("Create game response. Game :$game")
        return game.toGameResponse()
    }

    override fun getGames(request: Contract.Player): Flow<Contract.Game> {
        return this.getGames(request.playerId).toGameFlow()
    }

    override suspend fun move(request: Contract.Movement): Contract.MovementResponse {

        return this.move(request.gameId, request.currentPlayerId, request.movement)
            .toMovementResponse()
    }

    override fun getBoard(request: Contract.GetBoardRequest): Flow<Contract.Board> {
        return this.getBoard(request.gameId).toBoardFlow()
    }

    private fun Flow<Board>.toBoardFlow(): Flow<Contract.Board> {
        return this.map {
            buildContractBoard(it.pieces)
        }
    }

    private fun Flow<Game>.toGameFlow(): Flow<Contract.Game> {
        return this.map { game ->
            Contract.Game.newBuilder()
                .setGameId(game.id)
                .setCurrentPlayer(Contract.Player.newBuilder()
                    .setPlayerId(game.currentPlayer.id)
                    .build()
                )
                .setBoard(buildContractBoard(game.pieces))
                .build()
        }
    }

    private fun buildContractBoard(pieces: List<List<Piece>>):Contract.Board {
        val boardBuilder = Contract.Board.newBuilder()
        pieces.forEachIndexed { i, list ->
            list.forEachIndexed { j, piece ->
                val currentPosition = Pair(i, j)
                boardBuilder.addCell(Contract.Cell.newBuilder()
                    .setCellId(mapCell(currentPosition))
                    .setPiece(Contract.Piece.newBuilder()
                        .setName(piece.toString())
                        .setColor(mapColor(piece))
                    )
                )
            }
        }
        return boardBuilder.build()
    }

    private fun mapCell(currentPosition: Pair<Int, Int>): String {
        val xNumbers = mapOf(
            0 to 'A',
            1 to 'B',
            2 to 'C',
            3 to 'D',
            4 to 'E',
            5 to 'F',
            6 to 'G',
            7 to 'H'
        )
        return xNumbers[currentPosition.second]!! + (currentPosition.first + 1).toString()
    }

    private fun mapColor(piece: Piece): Contract.Piece.Color {
        return when (piece.color) {
            Piece.Color.BLACK -> Contract.Piece.Color.BLACK
            Piece.Color.WHITE -> Contract.Piece.Color.WITHE
            Piece.Color.NONE -> Contract.Piece.Color.NONE
        }
    }

    private fun Result<Game, GameError>.toGameResponse(): Contract.GameResponse {
        return when (this) {
            is Result.Ok -> Contract.GameResponse.newBuilder()
                .setStatus(Contract.GameResponse.Status.SUCCESS)
                .build()
            is Result.Error -> Contract.GameResponse.newBuilder()
                .setStatus(Contract.GameResponse.Status.ERROR)
                .build()
        }
    }

    private fun Player.toPlayer(): Contract.Player {
        return Contract.Player
            .newBuilder()
            .setPlayerId(this.id)
            .build()
    }
}

private fun Boolean.toMovementResponse(): Contract.MovementResponse {
    return Contract.MovementResponse.newBuilder().setAllowed(this).build()
}

package com.etermax.blackops_chess

import com.etermax.blackops_chess.api.ChessApi
import io.grpc.Server
import io.grpc.ServerBuilder

class ChessServer(private val port: Int, chessApi: ChessApi) {

    private val server: Server = ServerBuilder
        .forPort(port)
        .addService(chessApi)
        .build()

    fun start() {
        server.start()
        println("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                println("*** shutting down gRPC server since JVM is shutting down")
                this@ChessServer.stop()
                println("*** server shut down")
            }
        )
    }

    private fun stop() = server.shutdown()

    fun blockUntilShutdown() {
        server.awaitTermination()
    }

}


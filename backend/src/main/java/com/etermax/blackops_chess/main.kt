package com.etermax.blackops_chess

import com.etermax.blackops_chess.actions.*
import com.etermax.blackops_chess.api.ChessApi
import com.etermax.blackops_chess.domain.board.BoardGenerator
import com.etermax.blackops_chess.domain.game.GameService
import com.etermax.blackops_chess.domain.player.PlayerService
import com.etermax.blackops_chess.infrastructure.IdGenerator
import com.etermax.blackops_chess.infrastructure.repositories.GameRepositoryFactory
import com.etermax.blackops_chess.infrastructure.repositories.PlayerRepositoryFactory

fun main() {
    val port = 8888
    val chessApi = buildChessApi()
    val server = ChessServer(port, chessApi)
    server.start()
    server.blockUntilShutdown()
}

private fun buildChessApi(): ChessApi {
    val playerService = playerService()
    val gameService = gameService(playerService)
    return ChessApi(
        CreatePlayer(playerService),
        CreateGame(gameService),
        GetGames(gameService),
        Move(gameService),
        GetBoard(gameService)
    )
}

private fun gameService(playerService: PlayerService): GameService {
    val repository = GameRepositoryFactory.create()
    return GameService(repository, playerService, IdGenerator(), BoardGenerator())
}

private fun playerService(): PlayerService {
    val repository = PlayerRepositoryFactory.create()
    return PlayerService(IdGenerator(), repository)
}
package com.etermax.blackops_chess.infrastructure.repositories

import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.game.GameError
import com.etermax.blackops_chess.domain.game.GameRepository
import com.etermax.blackops_chess.utils.Result

class InMemoryGameRepository : GameRepository {

    private val games = mutableSetOf<Game>()

    override fun save(game: Game) {
        games.add(game)
    }

    override fun findById(id: String): Result<Game, GameError> {
        val game = games.find { game -> game.id == id }
        return if (game == null) Result.Error(GameError) else Result.Ok(game)
    }

    override fun findByPlayerId(playerId: String): List<Game> {
        return games.filter { it.player1.id == playerId || it.player2.id == playerId }
    }

    override fun update(game: Game) {
        games.find { it.id == game.id }?.let {
            games.remove(it)
            games.add(game)
        }
    }
}

object GameRepositoryFactory {
    fun create(): GameRepository = InMemoryGameRepository()
}
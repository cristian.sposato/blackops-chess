package com.etermax.blackops_chess.infrastructure

import java.util.*

class IdGenerator {
    fun generate(): String {
        return UUID.randomUUID().toString()
    }
}

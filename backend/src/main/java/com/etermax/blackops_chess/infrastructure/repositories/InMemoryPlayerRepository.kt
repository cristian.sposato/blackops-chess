package com.etermax.blackops_chess.infrastructure.repositories

import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.domain.player.PlayerError
import com.etermax.blackops_chess.domain.player.PlayerRepository
import com.etermax.blackops_chess.utils.Result

class InMemoryPlayerRepository : PlayerRepository {
    private val players = mutableListOf<Player>(Player("player1"), Player("player2"))

    override fun save(player: Player) {
        players.add(player)
    }

    override fun findById(id: String): Result<Player, PlayerError> {
        val result = players.find { player -> player.id == id }
        return if (result == null) Result.Error(PlayerError()) else Result.Ok(result)
    }
}

object PlayerRepositoryFactory {
    fun create(): PlayerRepository = InMemoryPlayerRepository()
}
package com.etermax.blackops_chess.domain.board

import com.etermax.blackops_chess.domain.board.Piece.*
import com.etermax.blackops_chess.utils.emptyBoard
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RookPieceTest {
    companion object {
        val ORIGIN = Pair(0,0)
        val DESTINATION = Pair(0,1)
        val WRONG_DESTINATION = Pair(1,1)
    }

    private lateinit var rook: RookPiece

    @Before
    fun setUp() {
        rook = RookPiece(Color.BLACK)
    }

    @Test
    fun `rook valid move return true`() {
        val canMove = rook.canMove(emptyList(),ORIGIN, DESTINATION, )
        val canNotMove = rook.canMove(emptyList(), ORIGIN, WRONG_DESTINATION, )

        assertEquals(true, canMove)
        assertEquals(false, canNotMove)
    }

    @Test
    fun `move through a non-empty trajectory`() {
        val board = emptyBoard(3) {
            it[0][0] = rook
            it[0][1] = RookPiece(Color.WHITE)
        }

        val result = rook.canMove(board, ORIGIN, Pair(0, 2))

        assertEquals(false, result)
    }

    @Test
    fun `move through an empty trajectory`() {
        val board = emptyBoard(3) {
            it[0][0] = rook
        }

        val result = rook.canMove(board, ORIGIN, Pair(0, 2))

        assertEquals(true, result)
    }
}
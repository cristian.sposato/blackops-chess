package com.etermax.blackops_chess.domain.board

import com.etermax.blackops_chess.domain.board.Piece.Color
import com.etermax.blackops_chess.utils.emptyBoard
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BoardTest {

    companion object {
        const val MOVE = "A1A2"
        const val INVALID_MOVE = "A1A1"
        const val OTHER_INVALID_MOVE = "M1J1"
        val ORIGIN_COORDINATES = Pair(0, 0)
        val DESTINATION_COORDINATES = Pair(0, 1)
    }

    @Mock lateinit var piece: Piece
    @Mock lateinit var otherPiece: Piece

    private lateinit var board: Board

    @Before
    fun setUp() {
    }

    @Test
    fun `move piece`() {

        val initialPieces = emptyBoard {
            it[0][0] = piece
        }
        val finalPieces = emptyBoard {
            it[0][1] = piece
        }
        given(piece.canMove(
            initialPieces,
            ORIGIN_COORDINATES,
            DESTINATION_COORDINATES,
        )).willReturn(true)
        given(piece.color).willReturn(Color.BLACK)

        board = Board(initialPieces)
        val moveSucceeded = board.move(MOVE)

        val after = board.pieces
        assertEquals(true, moveSucceeded)
        finalPieces.forEachIndexed { i, arrayOfPieces ->
            arrayOfPieces.forEachIndexed { j, piece ->
                assertEquals(piece, after[i][j])
            }
        }
    }

    @Test
    fun `try to move a piece to the same place`() {
        val pieces = emptyBoard {
            it[0][0] = piece
        }
        board = Board(pieces)

        val moveFailed = board.move(INVALID_MOVE)
        val otherMoveFailed = board.move(OTHER_INVALID_MOVE)

        assertEquals(false, moveFailed)
        assertEquals(false, otherMoveFailed)
    }

    @Test
    fun `try to capture a same color piece`() {
        val pieces = emptyBoard {
            it[0][0] = piece
            it[0][1] = otherPiece
        }
        given(piece.canMove(
            pieces,
            ORIGIN_COORDINATES,
            DESTINATION_COORDINATES,
        )).willReturn(true)
        given(piece.color).willReturn(Color.BLACK)
        given(otherPiece.color).willReturn(Color.BLACK)
        board = Board(pieces)

        val moveResult = board.move(MOVE)

        assertEquals(false, moveResult)
    }
}
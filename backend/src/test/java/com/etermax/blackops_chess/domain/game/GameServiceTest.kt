package com.etermax.blackops_chess.domain.game

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.domain.player.PlayerService
import com.etermax.blackops_chess.infrastructure.IdGenerator
import com.etermax.blackops_chess.domain.player.PlayerError
import com.etermax.blackops_chess.domain.board.BoardGenerator
import com.etermax.blackops_chess.domain.board.Piece
import com.etermax.blackops_chess.domain.board.RookPiece
import com.etermax.blackops_chess.utils.Result
import com.etermax.blackops_chess.utils.emptyBoard
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GameServiceTest {

    @Mock lateinit var playerService: PlayerService
    @Mock lateinit var idGenerator: IdGenerator
    @Mock lateinit var gameRepository: GameRepository
    @Mock lateinit var boardGenerator: BoardGenerator

    private lateinit var gameService: GameService

    companion object {
        private const val PLAYER_ID1 = "player 1"
        private const val PLAYER_ID2 = "player 2"
        private val PLAYER1 = Player(PLAYER_ID1)
        private val PLAYER2 = Player(PLAYER_ID2)
        private const val GAME_ID1 = "game_id1"
        private const val GAME_ID2 = "game_id2"
        private const val NEW_GAME_ID = "new_game_id"
        private val BOARD = Board()
        private val GAME1 = Game(GAME_ID1, PLAYER1, PLAYER2, BOARD, PLAYER1)
        private val GAME2 = Game(GAME_ID2, PLAYER1, PLAYER2, BOARD, PLAYER1)
        val NEW_GAME = Game(NEW_GAME_ID, PLAYER1, PLAYER2, BOARD, PLAYER1)
        val GAMES = listOf(GAME1, GAME2)
        const val MOVE = "a8a7"
        val MOVES = listOf(MOVE)
        val VALID_MOVE = true
    }

    @Mock
    lateinit var board: Board

    @Before
    fun setup() {
        gameService = GameService(gameRepository, playerService, idGenerator, boardGenerator)
    }

    @Test
    fun `with existing players return a Game`() {
        given(playerService.findById(PLAYER_ID1)).willReturn(Result.Ok(PLAYER1))
        given(playerService.findById(PLAYER_ID2)).willReturn(Result.Ok(PLAYER2))
        given(idGenerator.generate()).willReturn(GAME_ID1)
        given(boardGenerator.generate()).willReturn(BOARD)

        val game = gameService.createGame(PLAYER_ID1, PLAYER_ID2)

        verify(gameRepository).save(GAME1)
        assertEquals(true, game is Result.Ok)
        assertEquals(GAME1, (game as Result.Ok).value)
        assertEquals(PLAYER1, game.value.currentPlayer)
    }

    @Test
    fun `with non-existing players return an error`() {
        given(playerService.findById(PLAYER_ID1)).willReturn(Result.Ok(PLAYER1))
        given(playerService.findById(PLAYER_ID2)).willReturn(Result.Error(PlayerError()))

        val game = gameService.createGame(PLAYER_ID1, PLAYER_ID2)

        verify(gameRepository, never()).save(GAME1)
        assertEquals(true, game is Result.Error)
        assertEquals(GameError, (game as Result.Error).error)
    }

    @Test
    fun `return an error when player1 is equals to player2`() {
        val result = gameService.createGame(PLAYER_ID1, PLAYER_ID1)

        assertEquals(true, result is Result.Error)
        assertEquals(GameError, (result as Result.Error).error)
    }

    @Test
    fun `return games for a given player`() = runBlockingTest {
        given(gameRepository.findByPlayerId(PLAYER_ID1)).willReturn(GAMES)

        val outputList = mutableListOf<Game>()
        gameService.getGames(PLAYER_ID1).onEach {
            outputList.add(it)
        }.first {
            outputList.size >= 2
        }

        assertEquals(GAMES, outputList)
    }

    @Test
    fun `when create a new game then return all games`() = runBlockingTest {
        given(gameRepository.findByPlayerId(PLAYER_ID1)).willReturn(GAMES)

        // create new
        given(playerService.findById(PLAYER_ID1)).willReturn(Result.Ok(PLAYER1))
        given(playerService.findById(PLAYER_ID2)).willReturn(Result.Ok(PLAYER2))
        given(idGenerator.generate()).willReturn(NEW_GAME_ID)
        given(boardGenerator.generate()).willReturn(BOARD)
        gameService.createGame(PLAYER_ID1, PLAYER_ID2)

        val outputList = mutableListOf<Game>()
        gameService.getGames(PLAYER_ID1).onEach {
            outputList.add(it)
        }.first {
            outputList.size >= 3
        }

        assertEquals(GAMES + NEW_GAME, outputList)
    }

    @Test
    fun `move a piece in board game`() {
        val game = Game(GAME_ID1, PLAYER1, PLAYER2, board, PLAYER1)
        given(gameRepository.findById(GAME_ID1)).willReturn(Result.Ok(game))
        given(board.move(MOVE.uppercase())).willReturn(VALID_MOVE)

        val moveSucceeded = gameService.move(GAME_ID1, PLAYER_ID1, MOVE)

        assertEquals(true, moveSucceeded)
        verify(gameRepository).update(game.copy(currentPlayer = PLAYER2))
    }

    @Test
    fun `after move update current game`() {
        val game = Game(GAME_ID1, PLAYER1, PLAYER2, board, PLAYER1)
        given(gameRepository.findById(GAME_ID1)).willReturn(Result.Ok(game))
        given(board.move(MOVE.uppercase())).willReturn(VALID_MOVE)

        val result = gameService.move(GAME_ID1, PLAYER_ID1, MOVE)

        assertEquals(VALID_MOVE, result)
        verify(gameRepository).update(game.copy(currentPlayer = PLAYER2))
    }

    @Test
    fun `move fails when player is not the current player`() {
        val game = Game(GAME_ID1, PLAYER1, PLAYER2, board, PLAYER1)
        given(gameRepository.findById(GAME_ID1)).willReturn(Result.Ok(game))

        val result = gameService.move(GAME_ID1, PLAYER_ID2, MOVE)

        assertEquals(false, result)
    }

    @Test
    fun `get board for an existing game`() = runBlockingTest {
        val game = Game(GAME_ID1, PLAYER1, PLAYER2, board, PLAYER1)
        given(gameRepository.findById(GAME_ID1)).willReturn(Result.Ok(game))

        val outputList = mutableListOf<Board>()
        gameService.getBoard(GAME_ID1).onEach {
            outputList.add(it)
        }.first()

        assertEquals(board, outputList.first())
    }

    @Test
    fun `get board after a move in an existing game`() = runBlockingTest {
        val piece = RookPiece(Piece.Color.WHITE)
        val move = "A1A2"
        val currentBoard = Board(emptyBoard (2){
            it[0][0] = piece
        })
        val finalPieces = emptyBoard {
            it[0][1] = piece
        }
        val game = Game(GAME_ID1, PLAYER1, PLAYER2, currentBoard, PLAYER1)
        given(gameRepository.findById(GAME_ID1)).willReturn(Result.Ok(game))
        gameService.move(GAME_ID1, PLAYER_ID1, move)

        val outputList = mutableListOf<Board>()
        gameService.getBoard(GAME_ID1).onEach {
            outputList.add(it)
        }.first()

        assertEquals(finalPieces, outputList.first().pieces)
    }
}
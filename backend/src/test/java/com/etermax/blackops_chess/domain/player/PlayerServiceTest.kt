package com.etermax.blackops_chess.domain.player

import com.etermax.blackops_chess.infrastructure.IdGenerator
import com.etermax.blackops_chess.utils.Result
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PlayerServiceTest {
    companion object {
        const val ID_GENERATED = "SOME_ID"
        val PLAYER = Player(ID_GENERATED)
        val PLAYER_OK = Result.Ok(PLAYER)
    }
    @Mock lateinit var repository: PlayerRepository
    @Mock lateinit var idGenerator: IdGenerator

    private lateinit var service: PlayerService

    @Before
    fun setUp() {
        service = PlayerService(idGenerator, repository)
    }

    @Test
    fun `create player should save in repository`() {
        // given
        given(idGenerator.generate()).willReturn(ID_GENERATED)

        // when
        service.createPlayer()

        // then
        verify(repository).save(PLAYER)
        verify(idGenerator).generate()
    }

    @Test
    fun `return selected player by id`() {
        given(repository.findById(ID_GENERATED)).willReturn(PLAYER_OK)

        val result = service.findById(ID_GENERATED)

        verify(repository).findById(ID_GENERATED)
        assertEquals(true, result is Result.Ok)
        assertEquals(PLAYER_OK, result)
    }
}
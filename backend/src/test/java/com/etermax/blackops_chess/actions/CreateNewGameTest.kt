package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.domain.game.GameError
import com.etermax.blackops_chess.domain.game.GameService
import com.etermax.blackops_chess.utils.Result
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CreateNewGameTest {

    companion object {
        const val PLAYER_ID1 = "player 1"
        const val PLAYER_ID2 = "player 2"
        val PLAYER1 = Player(PLAYER_ID1)
        val GAME_OK = Result.Ok(Game("id", PLAYER1, Player(PLAYER_ID2), Board(), PLAYER1))
        val GAME_ERROR = Result.Error(GameError)
    }

    @Mock
    lateinit var gameService: GameService

    private lateinit var createGame: CreateGame

    @Before
    fun setUp() {
        createGame = CreateGame(gameService)
    }

    @Test
    fun `return a new game`() {
        given(gameService.createGame(PLAYER_ID1, PLAYER_ID2)).willReturn(GAME_OK)

        val game = createGame(PLAYER_ID1, PLAYER_ID2)

        verify(gameService).createGame(PLAYER_ID1, PLAYER_ID2)
        assertEquals(GAME_OK, game)
    }

    @Test
    fun `with non-existing players return an error`() {
        given(gameService.createGame(PLAYER_ID1, PLAYER_ID2)).willReturn(GAME_ERROR)

        val game = createGame(PLAYER_ID1, PLAYER_ID2)

        verify(gameService).createGame(PLAYER_ID1, PLAYER_ID2)
        assertEquals(GAME_ERROR, game)
    }
}
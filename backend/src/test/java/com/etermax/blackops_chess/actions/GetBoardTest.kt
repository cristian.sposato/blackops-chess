package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.game.GameService
import com.etermax.blackops_chess.domain.player.Player
import kotlinx.coroutines.flow.flowOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetBoardTest {

    companion object {
        private const val GAME_ID = "game_id"
    }

    lateinit var getBoard: GetBoard

    @Mock
    lateinit var gameService: GameService

    @Mock
    lateinit var board: Board

    @Before
    fun setup() {
        getBoard = GetBoard(gameService)
    }

    @Test
    fun `return board for current game`() {
        val boardFlow = flowOf(board)
        given(gameService.getBoard(GAME_ID)).willReturn(boardFlow)

        val result = getBoard(GAME_ID)

        Assert.assertEquals(boardFlow, result)
    }

}
package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.domain.player.PlayerService
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CreatePlayerTest {
    companion object {
        val PLAYER = Player("")
    }
    @Mock lateinit var playerService: PlayerService

    @Test
    fun `create new player to server`() {
        // given
        val createPlayer = CreatePlayer(playerService)
        given(playerService.createPlayer()).willReturn(PLAYER)

        // when
        val player = createPlayer()

        // then
        verify(playerService).createPlayer()
        Assert.assertEquals(PLAYER, player)
    }
}


package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.game.GameService
import com.etermax.blackops_chess.domain.player.Player
import kotlinx.coroutines.flow.flowOf
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetGamesTest {

    companion object {
        private val PLAYER1 = Player("player_id1")
        private val PLAYER2 = Player("player_id2")
        private const val GAME_ID = "game_id"
        private val FLOW_GAMES = flowOf(Game(GAME_ID, PLAYER1, PLAYER2, Board(), PLAYER1))
    }

    lateinit var getGames: GetGames

    @Mock
    lateinit var gameService: GameService

    @Before
    fun setup() {
        getGames = GetGames(gameService)
    }

    @Test
    fun `return current games for current player`() {
        given(gameService.getGames(PLAYER1.id)).willReturn(FLOW_GAMES)

        val result = getGames(PLAYER1.id)

        assertEquals(FLOW_GAMES, result)
    }
}
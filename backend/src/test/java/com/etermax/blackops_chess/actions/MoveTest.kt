package com.etermax.blackops_chess.actions

import com.etermax.blackops_chess.domain.game.GameService
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MoveTest {

    companion object {
        const val GAME_ID = "game_id"
        const val MOVE = "a8a7"
        const val VALID_MOVE = true
        const val CURRENT_PLAYER_ID = "current"
        val MOVES = listOf("a8a7")
    }

    lateinit var move: Move

    @Mock
    lateinit var gameService: GameService

    @Before
    fun setup() {
        move = Move(gameService)
    }

    @Test
    fun `move a piece return a valid response`() {
        given(gameService.move(GAME_ID, CURRENT_PLAYER_ID, MOVE)).willReturn(VALID_MOVE)

        val actual = move(GAME_ID, CURRENT_PLAYER_ID, MOVE)

        assertEquals(VALID_MOVE, actual)
    }
}
package com.etermax.blackops_chess.infrastructure.repositories

import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.domain.player.PlayerRepository
import com.etermax.blackops_chess.utils.Result
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PlayerRepositoryTest {
    companion object {
        val PLAYER = Player("SOME_ID")
    }
    private lateinit var playerRepository: PlayerRepository

    @Before
    fun setUp() {
        playerRepository = PlayerRepositoryFactory.create()
    }

    @Test
    fun `when player is created should get saved`() {
        playerRepository.save(PLAYER)

        val actual = playerRepository.findById(PLAYER.id)
        assertEquals(true, actual is Result.Ok)
        assertEquals(PLAYER, (actual as Result.Ok).value)
    }
}
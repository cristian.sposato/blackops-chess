package com.etermax.blackops_chess.infrastructure.repositories

import com.etermax.blackops_chess.domain.board.Board
import com.etermax.blackops_chess.domain.game.Game
import com.etermax.blackops_chess.domain.game.GameRepository
import com.etermax.blackops_chess.domain.player.Player
import com.etermax.blackops_chess.utils.Result
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class GameRepositoryTest {

    companion object {
        private const val PLAYER_ID1 = "p1"
        private val BOARD = Board()
        private val PLAYER1 = Player(PLAYER_ID1)
        private val PLAYER2 = Player("p2")
        val GAME = Game("id", PLAYER1, PLAYER2, BOARD, PLAYER1)
        val GAME2 = Game("id", PLAYER2, PLAYER1, BOARD, PLAYER2)
    }

    private lateinit var gameRepository: GameRepository

    @Before
    fun setUp() {
         gameRepository = GameRepositoryFactory.create()
    }

    @Test
    fun `save game created`() {
        gameRepository.save(GAME)

        val actual = gameRepository.findById(GAME.id)
        assertEquals(true, actual is Result.Ok)
        assertEquals(GAME, (actual as Result.Ok).value)
    }

    @Test
    fun `return games for a given player`() {
        gameRepository.save(GAME)
        gameRepository.save(GAME2)

        val actual = gameRepository.findByPlayerId(PLAYER_ID1)
        assertEquals(listOf(GAME, GAME2), actual)
    }
}
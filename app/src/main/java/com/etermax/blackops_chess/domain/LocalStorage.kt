package com.etermax.blackops_chess.domain

interface LocalStorage {
    fun getPlayer(): Player?
    fun setPlayer(player: Player)
}
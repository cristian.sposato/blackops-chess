package com.etermax.blackops_chess.domain

import com.etermax.blackops_chess.domain.board.Cell
import kotlinx.coroutines.flow.Flow

interface ChessService {
    suspend fun createPlayer(): Player
    suspend fun move(gameId: String, currentPlayerId: String, move: String): Boolean
    suspend fun createNewGame(player1Id: String, player2Id: String): Boolean
    suspend fun getGames(playerId: String): Flow<Game>
    suspend fun getBoard(gameId: String): Flow<Map<String, Cell>>
}
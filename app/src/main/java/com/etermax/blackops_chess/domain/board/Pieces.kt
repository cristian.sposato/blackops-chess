package com.etermax.blackops_chess.domain.board

object Pieces {

    fun buildBoard(source: Map<String, Cell>): LinkedHashMap<String, Cell> {
        val result = createEmptyBoard()

        source.forEach {
            result[it.key] = it.value
        }

        return result
    }

    private fun createEmptyBoard() = linkedMapOf<String, Cell>().apply {
        for (i in 8 downTo 1 step 1) {
            for (j in 'A'..'H') {
                this["$j$i"] = Cell("", null)
            }
        }
    }
}
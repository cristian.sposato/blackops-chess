package com.etermax.blackops_chess.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Player(val id: String) : Parcelable
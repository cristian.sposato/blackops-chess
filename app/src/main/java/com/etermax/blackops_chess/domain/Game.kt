package com.etermax.blackops_chess.domain

import android.os.Parcelable
import com.etermax.blackops_chess.domain.board.Cell
import kotlinx.parcelize.Parcelize

@Parcelize
data class Game(
    val id: String,
    val pieces: Map<String, Cell>,
    val currentPlayer: Player,
) : Parcelable
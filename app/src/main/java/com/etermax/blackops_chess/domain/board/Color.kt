package com.etermax.blackops_chess.domain.board

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class Color : Parcelable {
    NONE, BLACK, WHITE
}
package com.etermax.blackops_chess.domain.board

import java.security.InvalidParameterException

class ChessBoard(pieces: Map<String, Cell>) {

    private var board: LinkedHashMap<String, Cell> = Pieces.buildBoard(pieces)

    fun getCell(position: String): Cell {
        return board[position.uppercase()]!!
    }

    fun getCells() = board.values.toList()

    fun updateBoard(updatedBoard: LinkedHashMap<String, Cell>) {
        board = updatedBoard
    }

    fun move(movement: String): Boolean {
        val move = try {
            Movement(movement.uppercase())
        } catch (e: InvalidParameterException) {
            return false
        }

        val initialPosition = move.initialPosition
        val finalPosition = move.finalPosition
        val cell = getCell(initialPosition)
        val emptyCell = Cell("", null)
        board[finalPosition] = cell
        board[initialPosition] = emptyCell
        return true

    }

    fun getPosition(currentCell: Cell): String {
        return board.entries.find {
            it.value == currentCell
        }?.key!!
    }

    class Movement(movement: String) {
        var initialPosition: String
        var finalPosition: String

        init {
            if(!validateInput(movement)) throw InvalidParameterException()
            initialPosition = movement.substring(0..1)
            finalPosition = movement.substring(2..3)
        }

        private fun validateInput(movement: String): Boolean {
            val regex = "([A-H])[1-8]([A-H])[1-8]".toRegex()
            return regex.matches(movement)

        }
    }
}
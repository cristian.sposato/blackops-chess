package com.etermax.blackops_chess.domain.board

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cell(val value: String, val color: Color?) : Parcelable
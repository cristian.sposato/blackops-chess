package com.etermax.blackops_chess.board

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.etermax.blackops_chess.R
import com.etermax.blackops_chess.databinding.ActivityChessBoardBinding
import com.etermax.blackops_chess.domain.board.ChessBoard
import com.etermax.blackops_chess.domain.Game
import com.etermax.blackops_chess.infrastructure.GRPCChessService
import com.etermax.blackops_chess.infrastructure.PreferenceStorage
import com.etermax.blackops_chess.utils.GAME_KEY
import com.etermax.blackops_chess.utils.createSharedPreferences


class ChessBoardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val game = intent.getParcelableExtra<Game>(GAME_KEY) as Game

        val viewModel: ChessBoardViewModel by viewModels {
            ChessBoardViewModel.Factory(
                GRPCChessService(),
                ChessBoard(game.pieces),
                game.id,
                PreferenceStorage(createSharedPreferences()),
            )
        }

        val binding = ActivityChessBoardBinding.inflate(layoutInflater)

        val adapter = ChessAdapter(viewModel.getCells())

        binding.rvBoard.apply {
            layoutManager = GridLayoutManager(context, 8)
            this.adapter = adapter
        }

        binding.btnMove.setOnClickListener {
            val movement = binding.etMove.text.toString()
            viewModel.move(movement)
        }

        setContentView(binding.root)

        viewModel.refreshEvent.observe(this) { success ->
            if (success) {
                adapter.update(viewModel.getCells())
            } else {
                Toast.makeText(this, R.string.invalid_movement, Toast.LENGTH_SHORT).show()
            }
        }
    }
}


package com.etermax.blackops_chess.board

import androidx.lifecycle.*
import com.etermax.blackops_chess.domain.board.ChessBoard
import com.etermax.blackops_chess.domain.ChessService
import com.etermax.blackops_chess.domain.LocalStorage
import com.etermax.blackops_chess.domain.board.Pieces
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ChessBoardViewModel(
    private val service: ChessService,
    private val board: ChessBoard,
    private val gameId: String,
    private val localStorage: LocalStorage,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main.immediate,
) : ViewModel() {

    private val _refreshEvent = MutableLiveData<Boolean>()
    val refreshEvent: LiveData<Boolean> = _refreshEvent

    init {
        getBoard()
    }

    fun getCells() = board.getCells()

    fun move(movement: String) = viewModelScope.launch(dispatcher) {
        val playerId = localStorage.getPlayer()?.id ?: return@launch
        val successful = service.move(gameId, playerId, movement)

        if (successful) {
            board.move(movement)
        }
        _refreshEvent.value = successful
    }

    private fun getBoard() = viewModelScope.launch(dispatcher) {
        service.getBoard(gameId).collect {
            board.updateBoard(Pieces.buildBoard(it))
            _refreshEvent.value = true
        }
    }

    class Factory(
        private val chessService: ChessService,
        private val board: ChessBoard,
        private val gameId: String,
        private val localStorage: LocalStorage,
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ChessBoardViewModel(chessService, board, gameId, localStorage) as T
        }
    }
}


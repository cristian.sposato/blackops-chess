package com.etermax.blackops_chess.board

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etermax.blackops_chess.databinding.CellViewBinding
import com.etermax.blackops_chess.domain.board.Cell

class ChessAdapter(
    private var list: List<Cell>
) : RecyclerView.Adapter<ChessAdapter.ChessHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChessHolder {
        val binding = CellViewBinding.inflate(LayoutInflater.from(parent.context))
        return ChessHolder(binding)
    }

    override fun onBindViewHolder(holder: ChessHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size
    fun update(board: List<Cell>) {
        list = board
        notifyDataSetChanged()
    }

    inner class ChessHolder(private val binding: CellViewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(cell: Cell) {
            binding.tvName.text = cell.value
        }
    }
}

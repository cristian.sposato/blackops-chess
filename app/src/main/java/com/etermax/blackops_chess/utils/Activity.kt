package com.etermax.blackops_chess.utils

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

fun Activity.createSharedPreferences(): SharedPreferences {
    return getSharedPreferences("main", MODE_PRIVATE)
}
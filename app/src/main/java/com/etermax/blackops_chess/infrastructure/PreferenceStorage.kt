package com.etermax.blackops_chess.infrastructure

import android.content.SharedPreferences
import androidx.core.content.edit
import com.etermax.blackops_chess.domain.LocalStorage
import com.etermax.blackops_chess.domain.Player

class PreferenceStorage(private val sharedPreferences: SharedPreferences) : LocalStorage {

    companion object {
        private const val PLAYER_ID_KEY = "player_id_key"
    }

    override fun getPlayer(): Player? {
        return sharedPreferences.getString(PLAYER_ID_KEY, null)?.let { playerId ->
            Player(playerId)
        }
    }

    override fun setPlayer(player: Player) {
        sharedPreferences.edit {
            putString(PLAYER_ID_KEY, player.id)
        }
    }
}
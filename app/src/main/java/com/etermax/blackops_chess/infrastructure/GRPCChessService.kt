package com.etermax.blackops_chess.infrastructure

import BoardStateGrpcKt
import Contract
import android.util.Log
import com.etermax.blackops_chess.domain.board.Cell
import com.etermax.blackops_chess.domain.board.Color
import com.etermax.blackops_chess.domain.ChessService
import com.etermax.blackops_chess.domain.Game
import com.etermax.blackops_chess.domain.Player
import io.grpc.ManagedChannelBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asExecutor
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.net.URL

class GRPCChessService : ChessService {
    companion object {
        val TAG = GRPCChessService::class.simpleName
    }

    private val grpcStub: BoardStateGrpcKt.BoardStateCoroutineStub by lazy {
        val url = URL("http://10.0.2.2:8888")
        val channel = ManagedChannelBuilder.forAddress(url.host, url.port)
            .usePlaintext()
            .executor(Dispatchers.Default.asExecutor())
            .build()
        BoardStateGrpcKt.BoardStateCoroutineStub(channel)
    }

    override suspend fun createPlayer(): Player {
        val client = Contract.Client.newBuilder().build()
        Log.i(TAG, "Create player request.")
        val playerId = grpcStub.createPlayer(client).playerId
        Log.i(TAG, "Create player response. PlayerID:$playerId")
        return Player(playerId)
    }

    override suspend fun createNewGame(player1Id: String, player2Id: String): Boolean {
        val player1 = Contract.Player.newBuilder().setPlayerId(player1Id).build()
        val player2 = Contract.Player.newBuilder().setPlayerId(player2Id).build()
        val players = Contract.Players.newBuilder()
            .setPlayer1(player1)
            .setPlayer2(player2)
            .build()
        Log.i(TAG, "Create game request with params $players")
        val response = grpcStub.createNewGame(players).status
        Log.i(TAG, "Create game response. Game:$response")
        return response == Contract.GameResponse.Status.SUCCESS
    }

    override suspend fun getGames(playerId: String): Flow<Game> {
        val request = Contract.Player.newBuilder().setPlayerId(playerId).build()
        return grpcStub.getGames(request).map { game ->
            val board = mapCellsToPieces(game.board)

            Log.i(TAG, "Get games response. Game:$game")
            val result = Game(game.gameId, board, Player(game.currentPlayer.playerId))
            Log.i(TAG, "Get games response. board:$board")
            result
        }
    }

    override suspend fun getBoard(gameId: String): Flow<Map<String, Cell>> {
        val request = Contract.GetBoardRequest.newBuilder().setGameId(gameId).build()

        return grpcStub.getBoard(request).map { board ->
            mapCellsToPieces(board)
        }
    }

    override suspend fun move(gameId: String, currentPlayerId: String, move: String): Boolean {
        val request = Contract.Movement.newBuilder()
            .setGameId(gameId)
            .setCurrentPlayerId(currentPlayerId)
            .setMovement(move)
            .build()
        return grpcStub.move(request).allowed
    }
    private fun mapCellsToPieces(contractBoard: Contract.Board): LinkedHashMap<String, Cell> {
        val board = linkedMapOf<String, Cell>()
        contractBoard.cellList.forEach { cell ->
            board[cell.cellId] = Cell(cell.piece.name, mapToDomainColor(cell.piece.color))
        }
        return board
    }

    private fun mapToDomainColor(color: Contract.Piece.Color?): Color {
        return color!!.let {
            when (it) {
                Contract.Piece.Color.BLACK -> Color.BLACK
                Contract.Piece.Color.WITHE -> Color.WHITE
                else -> Color.NONE
            }
        }
    }
}
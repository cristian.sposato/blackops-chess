package com.etermax.blackops_chess.home

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.etermax.blackops_chess.board.ChessBoardActivity
import com.etermax.blackops_chess.databinding.ActivityHomeBinding
import com.etermax.blackops_chess.domain.Game
import com.etermax.blackops_chess.infrastructure.GRPCChessService
import com.etermax.blackops_chess.infrastructure.PreferenceStorage
import com.etermax.blackops_chess.utils.GAME_KEY
import com.etermax.blackops_chess.utils.createSharedPreferences


class HomeActivity : AppCompatActivity() {

    private val viewModel: HomeViewModel by viewModels {
        HomeViewModel.Factory(
            GRPCChessService(),
            PreferenceStorage(createSharedPreferences()),
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityHomeBinding.inflate(layoutInflater)

        binding.btnPlay.setOnClickListener {
            viewModel.createGame(binding.etPlayer2.text.toString()) { stringRes ->
                Toast.makeText(this, stringRes, Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnCopy.setOnClickListener {
            val clipboard: ClipboardManager =
                getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("myPlayerId", binding.tvPlayerId.text)
            clipboard.setPrimaryClip(clip)
            Toast.makeText(this, "Id copied!", Toast.LENGTH_SHORT).show()
        }

        setContentView(binding.root)

        viewModel.playerLiveData.observe(this) { player ->
            binding.tvPlayerId.text = player.id
            binding.btnPlay.isEnabled = true
        }

        viewModel.gameLiveData.observe(this) { game ->
            openChessBoardActivity(game)
        }
    }

    private fun openChessBoardActivity(game: Game) {
        val intent = Intent(this, ChessBoardActivity::class.java)
        intent.putExtra(GAME_KEY, game)
        startActivity(intent)
    }
}
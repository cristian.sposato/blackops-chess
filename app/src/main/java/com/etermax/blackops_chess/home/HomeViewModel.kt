package com.etermax.blackops_chess.home

import androidx.lifecycle.*
import com.etermax.blackops_chess.R
import com.etermax.blackops_chess.domain.ChessService
import com.etermax.blackops_chess.domain.Game
import com.etermax.blackops_chess.domain.LocalStorage
import com.etermax.blackops_chess.domain.Player
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel(
    private val chessService: ChessService,
    private val localStorage: LocalStorage,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main.immediate,
) : ViewModel() {

    private val _gameLiveData = MutableLiveData<Game>()
    val gameLiveData: LiveData<Game> = _gameLiveData
    private val _playerLiveData = MutableLiveData<Player>()
    val playerLiveData: LiveData<Player> = _playerLiveData
    private lateinit var player: Player

    init {
        viewModelScope.launch(dispatcher) {
            getPlayer().join()
            getGames()
        }
    }

    fun createGame(
        player2: String,
        onError: (stringRes: Int) -> Unit,
    ) = viewModelScope.launch(dispatcher) {
        val success = chessService.createNewGame(player.id, player2)
        if (!success) onError(R.string.game_creation_error)
    }

    private fun getPlayer() = viewModelScope.launch(dispatcher) {
        val player = localStorage.getPlayer()
        this@HomeViewModel.player = player ?: chessService.createPlayer().apply {
            localStorage.setPlayer(this)
        }
        _playerLiveData.value = this@HomeViewModel.player
    }

    private fun getGames() = viewModelScope.launch(dispatcher) {
        chessService.getGames(this@HomeViewModel.player.id).collect { game ->
            _gameLiveData.value = game
        }
    }

    class Factory(
        private val chessService: ChessService,
        private val localStorage: LocalStorage
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return HomeViewModel(chessService, localStorage) as T
        }
    }
}
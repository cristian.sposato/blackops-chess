package com.etermax.blackops_chess.board

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etermax.blackops_chess.domain.board.ChessBoard
import com.etermax.blackops_chess.domain.ChessService
import com.etermax.blackops_chess.domain.LocalStorage
import com.etermax.blackops_chess.domain.Player
import com.etermax.blackops_chess.utils.FakeObserver
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ChessBoardViewModelTest {

    companion object {
        val PLAYER = Player("playerId")
        const val GAME_ID = "gameId"
    }

    @Mock lateinit var chessService: ChessService
    @Mock lateinit var board: ChessBoard
    @Mock lateinit var localStorage: LocalStorage

    private lateinit var viewModel: ChessBoardViewModel

    private val testCoroutineDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        viewModel = ChessBoardViewModel(
            chessService,
            board,
            GAME_ID,
            localStorage,
            testCoroutineDispatcher
        )
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun `when move trigger refresh event`() = testCoroutineDispatcher.runBlockingTest {
        val movement = "A1B1"
        val fakeObserver = FakeObserver<Boolean>()
        given(chessService.move(GAME_ID, PLAYER.id, movement)).willReturn(true)
        given(localStorage.getPlayer()).willReturn(PLAYER)
        viewModel.refreshEvent.observeForever(fakeObserver)

        viewModel.move(movement)

        verify(chessService).move(GAME_ID, PLAYER.id, movement)
        assertEquals(true, fakeObserver.history[0])
    }
}
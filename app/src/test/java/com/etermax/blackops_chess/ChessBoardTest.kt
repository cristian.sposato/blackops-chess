package com.etermax.blackops_chess

import com.etermax.blackops_chess.domain.board.Cell
import com.etermax.blackops_chess.domain.board.ChessBoard
import com.etermax.blackops_chess.domain.board.Color
import org.junit.Test

import org.junit.Assert.*

class ChessBoardTest {
    @Test
    fun `when movement is done the piece moves to destination`() {
        // given
        val board = ChessBoard(linkedMapOf(
            "A8" to Cell("T", Color.BLACK),
            "B8" to Cell("", null)
        ))
        val movement = "a8b8"
        val currentCell = board.getCell("a8")
        val finalPosition = "B8"

        // when
        board.move(movement)

        // then
        assertEquals(finalPosition, board.getPosition(currentCell))
    }

    @Test
    fun `when movement is invalid value the return false`() {
        // given
        val board = ChessBoard(linkedMapOf())
        val invalidMovement = "j1t7"
        val invalidMovementLong = "a3f6d"

        // then
        assertEquals(false, board.move(invalidMovement))
        assertEquals(false, board.move(invalidMovementLong))
    }

    @Test
    fun `when movement is valid value the return true`() {
        // given
        val board = ChessBoard(linkedMapOf(
            "A1" to Cell("T", Color.BLACK),
            "G7" to Cell("T", Color.BLACK)
        ))
        val validMovement = "a1g7"
        val validMovementUppercase = "a1G7"

        // then
        assertEquals(true, board.move(validMovement))
        assertEquals(true, board.move(validMovementUppercase))
    }
}


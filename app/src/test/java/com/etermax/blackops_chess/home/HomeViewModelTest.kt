package com.etermax.blackops_chess.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etermax.blackops_chess.domain.ChessService
import com.etermax.blackops_chess.domain.LocalStorage
import com.etermax.blackops_chess.domain.Game
import com.etermax.blackops_chess.domain.Player
import com.etermax.blackops_chess.utils.FakeObserver
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {
    companion object {
        const val PLAYER1_ID = "playerId1"
        const val PLAYER2_ID = "playerId2"
        val PLAYER = Player(PLAYER1_ID)
        val GAME = Game("new_game_id", emptyMap(), PLAYER)
    }

    @Mock lateinit var chessService: ChessService
    @Mock lateinit var localStorage: LocalStorage

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `there is no player then should create a new one`() = runBlockingTest {
        given(localStorage.getPlayer()).willReturn(null)
        given (chessService.createPlayer()).willReturn(PLAYER)

        createViewModel()

        verify(chessService).createPlayer()
        verify(localStorage).setPlayer(PLAYER)
    }

    @Test
    fun `there is player then should not create a new one`() = runBlockingTest {
        given(localStorage.getPlayer()).willReturn(PLAYER)

        createViewModel()

        verify(chessService, never()).createPlayer()
        verify(localStorage, never()).setPlayer(PLAYER)
    }

    @Test
    fun `when create new player should notify observers`() = runBlockingTest {
        given(localStorage.getPlayer()).willReturn(PLAYER)
        val fakeObserver = FakeObserver<Player>()

        val viewModel = createViewModel()
        viewModel.playerLiveData.observeForever(fakeObserver)

        assertEquals(PLAYER, fakeObserver.history[0])
    }

    @Test
    fun `call ChessService to create a new game`() = runBlockingTest {
        given(localStorage.getPlayer()).willReturn(PLAYER)
        given(chessService.createNewGame(PLAYER1_ID, PLAYER2_ID)).willReturn(true)
        val viewModel = createViewModel()

        viewModel.createGame(PLAYER2_ID) {}

        verify(chessService).createNewGame(PLAYER1_ID, PLAYER2_ID)
    }

    @Test
    fun `when create new game should notify observers`() = runBlockingTest {
        given(localStorage.getPlayer()).willReturn(PLAYER)
        given(chessService.createNewGame(PLAYER1_ID, PLAYER2_ID)).willReturn(true)
        given(chessService.getGames(PLAYER1_ID)).willReturn(flow { emit(GAME) })

        val viewModel = createViewModel()
        viewModel.createGame(PLAYER2_ID) {}

        verify(chessService).createNewGame(PLAYER1_ID, PLAYER2_ID)
    }

    @Test
    fun `should notify all games when getGames is called`() = testCoroutineDispatcher.runBlockingTest {
        given(localStorage.getPlayer()).willReturn(PLAYER)
        given(chessService.getGames(PLAYER1_ID)).willReturn(flow { emit(GAME) })

        val fakeObserver = FakeObserver<Game>()

        val viewModel = createViewModel()

        viewModel.gameLiveData.observeForever(fakeObserver)

        assertEquals(GAME, fakeObserver.history[0])
    }

    private fun createViewModel(): HomeViewModel {
        return HomeViewModel(chessService, localStorage, testCoroutineDispatcher)
    }
}
package com.etermax.blackops_chess.utils

import androidx.lifecycle.Observer

class FakeObserver<T> : Observer<T> {
    val history = mutableListOf<T>()

    override fun onChanged(t: T) {
        history.add(t)
    }
}